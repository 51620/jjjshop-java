package net.jjjshop.shop.controller.page.diy.items;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import net.jjjshop.shop.controller.page.diy.DiyItem;

/**
 * 优惠券组
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("coupon")
public class Coupon implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("diyItem")
    private DiyItem item;

    public Coupon(){
        this.item = new DiyItem();
        item.setName("优惠券组");
        item.setType("coupon");
        item.setGroup("shop");
        // 样式
        JSONObject style = new JSONObject();
        style.put("background", "#ffffff");
        style.put("paddingTop", 10);
        item.setStyle(style);

        // 参数
        JSONObject params = new JSONObject();
        params.put("limit", 5);
        item.setParams(params);

        // 默认数据
        JSONArray data = new JSONArray();
        JSONObject itemData = new JSONObject();
        itemData.put("color", "red");
        itemData.put("reducePrice", "10");
        itemData.put("minPrice", "100");
        // 2条数据
        data.add(itemData);
        data.add(itemData);
        item.setData(data);
    }
}
